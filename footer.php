<footer class="main-ftr pdtb-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="heading-h4 text-center">Get In Touch</h4>
                <hr class="hline"></hr>

                <div class="ftrlinks mt-4 d-flex justify-content-between">
                    <div class="ftrli">
                        <p class="ftrhead">Quick Links</p>
                        <div class="d-flex ftrdivde">
                            <ul class="pr-3">
                                <li class="mb-3"><a href="index.php">Home</a></li>
                                <li class="mb-3"><a href="aboutus.php">About Us</a></li>
                                <li class="mb-3"><a href="roomsandsuites.php">Rooms</a></li>
                                <li class="mb-3"><a href="events.php">Events</a></li>
                            </ul>
                            <ul class="px-3">
                                <li class="mb-3"><a href="amenities.php">Amenities</a></li>
                                <li class="mb-3"><a href="contactus.php">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="ftrli">
                        <p class="ftrhead">Address</p>
                        <div>
                            <P class="pr-2">
                            Forest Falls Resort (Pradhan Farms)
                            Kudsavre Village, Karjat Road
                            Vangani Station West (CR)
                            Dist. Thane 421503
                            </P>
                        </div>
                    </div>

                    <div class="ftrli">
                        <p class="ftrhead">Phone Number </p>
                        <div>
                            <ul>
                                <li><a href="tel:9820468844">Reservations - 9820468844/55</a></li>
                                <li><a href="https://wa.me/919820468844" target="_blank">What’s app - 9820468844</a></li>
                                <li><a href="tel:9890760885">Resort Manager - 9890760885</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="ftrli">
                        <p class="ftrhead">Email Address  </p>
                        <div>
                            <p><a href="mailto:forestfallsresort@gmail.com">forestfallsresort@gmail.com</a></p>
                        </div>
                    </div>

                    <div class="ftrli">
                        <p class="ftrhead">Follow Us</p>
                        <div>
                            <ul class="sclico">
                                <li><a><span class="fb-logo socialicons" style="background:url('./img/facebook.svg')"></span></a></li>
                                <li><a><span class="twitter-logo socialicons" style="background:url('./img/twitter.svg')"></span></a></li>
                                <li><a<span class="insta-logo socialicons" style="background:url('./img/instagram.svg')"></span></a></li>
                                <li><a><span class="youtube-logo socialicons" style="background:url('./img/youtube.svg')"></span></a></li>
                            </ul>
                        </div>
                    </div>

                </div>

                <hr class="hline"></hr>
                <P class="cpyrighttext text-center">Copyright © 2018 Forest Falls All rights reserved.</P>
                <p class="text-center mb-0">Crafted By <a class="ifinitext" target="_blank" href="https://infinitydsign.com/">Infinitydsign</a></p>
            </div>
        </div>
    </div>
</footer>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/jquery3.5.1.slim.min.js"></script>
<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</html>

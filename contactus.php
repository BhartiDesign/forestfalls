<?php include('header.php') ?>
<div class="contactus bg">
    <div class="bannerbg d-flex contctbg">
        <div class="bnnertbox">
            <h1 class="large-text">contact us</h1>
        </div>
    </div>
</div>

<div class="amibgclr">
<div class="getintuch">
    <div class="container">
        <div class="col-lg-12">
            <div class="row  py-5">
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="gitbox">
                        <h4 class="heading-h4 mb-3">Get In Touch</h4>
                        <p class="para-info">
                            If you need any help, please contact us or send us an email or go to our forum We are sure that you can receive our reply as soon as possible.
                        </p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="row ml-4">
                        <div class="col-lg-4 col-md-4 col-sm-12 vline">
                            <span class="iconconacts mb-2" style="background:url('./img/phone.svg')"></span>
                            <p class="contacthing para-info">Phone</p>
                            <p class="para-info"><a href="tel:9820468844">Inquiry - 9820468844/55</a></p>
                            <p class="para-info"><a href="https://wa.me/919820468844">What’s app- 9820468844</a></p>
                            <p class="para-info"><a href="tel:9890760885">Manager- 9890760885</a></p>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 vline">
                            <span class="iconconacts mb-2" style="background:url('./img/location.svg')"></span>
                            <p class="contacthing para-info">ADDRESS</p>
                            <p class="para-info">Forest Falls Resort (Pradhan Farms)
                                Kudsavre Village
                                Karjat Road, Vangani Station West (CR) Dist. Thane 421503
                            </p>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-12 vline">
                            <span class="iconconacts mb-2" style="background:url('./img/email.svg')"></span>
                            <p class="contacthing para-info">EMAIL</p>
                            <p class="para-info"><a href="mailto:forestfallsresort@gmail.com">forestfallsresort@gmail.com</a></p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


    <div class="container">
        <div class="row py-5">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="feedbackfrm">
                    <h3 class="heading-h3 text-left mb-4">Feedback Form</h3>
                    <p class="para-info">We are sure you will receive our reply as soon as possible.</p>
                    <div class="col-lg-12 mt-3 mb-5 p-0">
                        <input type="text" class="form-control" id="name" placeholder="Your Name">
                    </div>
                    <div class="row m-0">
                        <div class="col-lg-6 mt-3 mb-5 pr-3 pl-0">
                            <input type="email" class="form-control" id="email" placeholder="Your Email">
                        </div>

                        <div class="col-lg-6 mt-3 mb-5 pr-3 pl-0">
                            <input type="number" class="form-control" id="phoneno" placeholder="Phone">
                        </div>
                    </div>

                    <div class="col-lg-12 mt-3 mb-5 p-0">
                        <input type="text" class="form-control" id="subject" placeholder="Subject (Optional)">
                    </div>

                    <div class="col-lg-12 mt-3 mb-5 p-0">
                        <input type="text" class="form-control" id="msg" placeholder="Here goes your message">
                    </div>

                    <div>
                        <div class="btn btn-primary text-center vmorebtn mt-5">Send message
                            <!-- <span class="rightarw"></span> -->
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-8 col-md-12 col-sm-12">
                <iframe class="p-5 w-100" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3769.8781382167444!2d73.29825161442518!3d19.113001255776204!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7f2eaaaaaaaab%3A0xef3107363485e630!2sForest%20Falls%20Resort!5e0!3m2!1sen!2sin!4v1649860952008!5m2!1sen!2sin" height="750" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
        </div>
    </div>
</div>
</div>


<?php include('footer.php') ?>
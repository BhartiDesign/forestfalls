<?php include('header.php') ?>
<div class="amenitiesbg">
    <div class="bannerbg d-flex amnbg">
        <div class="bnnertbox text-center">
            <h1 class="large-text">Amenities</h1>
        </div>
    </div>
</div>


<div class="amibgclr">
    <div class="container py-5">
        <div class="row">
            <div class="amenihead text-center pb-5 w-100">
                <h3 class="heading-h3 fntblck mb-3">Amenities & Activities</h3>
                <p class="para-info">Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> Lorem Ipsum has been the industry's standard dummy</p>
            </div>

            <div class="col-lg-12">
                <div class="row align-items-center headercon">
                    <div class="col-lg-6 p-0">
                        <div class="ameniimg acbg"></div>
                    </div>
                    <div class="col-lg-6 aniabt">
                        <div class="amenidetails ml-5">
                            <h3 class="heading-h3 fntblck text-left mb-2">Accommodation Cottages- </h3>
                            <ul>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Capacity of 100 pax for an overnight stay</li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Capacity of 300 pax for a one day event</li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Designed for group stays</li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Deluxe, Superior and Family cottages available. Each cottage has a varying capacity of 2/4/6/8/15 pax.</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row flex-row-reverse align-items-center headercon">
                    <div class="col-lg-6 p-0">
                        <div class="ameniimg dinbg"></div>
                    </div>
                    <div class="col-lg-6 aniabt">
                        <div class="amenidetails">
                            <h3 class="heading-h3 fntblck text-left mb-2">Dining - </h3>
                            <p class="para-info mb-2">Experience our open air rustic restaurant and special sit-outs underneath mango trees </p>
                            <ul class="pl-3">
                                <li class="para-info mb-2"><span class="listdiamind"></span>Breakfast: Any 3 items - Egg bhurjee, Sheera, Pohe, Upma, Idli, Misal, Jalebi</li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Major Meals: Chicken preparation, Kadhai Paneer, Dal Tadka, Seasonal vegetable preparation, Chapati, Bhakri, Salad, Papad, Pickle, Dessert any 1 – Gulab jamun, Kheer, Fruit custard, Gajar halwa, Ice cream
                                    High Tea: Tea, Coffee and any 1 Snack -Batata vada, Bhel, Onion</li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>fritters</li>
                            </ul>
                            <p class="para-info mb-2">The resort also has an appetizer menu and items ordered will be on chargeable basis</p>
                        </div>
                    </div>
                </div>

                <div class="row  align-items-center headercon">
                    <div class="col-lg-6 p-0">
                        <div class="ameniimg actvibg"></div>
                    </div>
                    <div class="col-lg-6 aniabt">
                        <div class="amenidetails ml-5">
                            <h3 class="heading-h3 fntblck text-left mb-2">Activities - </h3>
                            <ul>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Walk around a dense 10 acre wadi. Find your ‘spot’ under any of our mango trees. And swaying palms
                                </li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Two Swimming pools, Water slides, Waterfall, Rain dance, River swimming.
                                </li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Indoor Games -Carom, Board Games, Chess, Lotto
                                </li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Outdoor games -Badminton Cricket, Football, Throw ball
                                </li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Cave Temple Visit <br>Rejuvenation spa sessions by a masseur/masseuse to leave you calm
                                </li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>and connected, is available on cal</li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Zumba, yoga, meditation sessions on prior reservation
                                </li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Experience a day in the life of the locals- a guided walk/drive around the village</li>
                                <li class="para-info mb-2"><span class="listdiamind"></span>Complimentary wi-fi for the perfect work from resort feels
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="facili text-center w-100 py-5">
            <p class="small-heading dmreg">OUR SERVICES</p>
            <h4 class="heading-h4 mb-4">Forest Hills Tata <br>Facilities & Amenities</h4>
        </div>

        <div class="col-lg-12">
            <div class="facliist d-flex">
                <ul>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/wifi.svg')"></span>
                            <p class="para-info mb-0 p-2 ml-3">Complimentary Wi-Fi</p>
                        </div>
                    </li>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/walkingperson.svg')"></span>
                            <p class="para-info mb-0 p-2 ml-3">Nature walks</p>
                        </div>
                    </li>
                  
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/dogpaw.svg')"></span>
                            <p class="para-infomb-0 p-2 ml-3">Pet Friendly </p>
                        </div>
                    </li>
                </ul>
                <ul>
                      <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/tv.svg')"></span>
                            <p class="para-info mb-0 p-2 ml-3">Cable TV</p>
                        </div>
                    </li>
                   
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/tea.svg')"></span>
                            <p class="para-info mb-0 p-2 ml-3">Restaurant & Cafe</p>
                        </div>
                    </li>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/distance.svg')"></span>
                            <p class="para-info mb-0 p-2 ml-3">Guided Historical Tours</p>
                        </div>
                    </li>

                </ul>

                <ul>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/swimmer.png')"></span>
                            <p class="para-info mb-0 p-2 ml-3">Wellness Experiences</p>
                        </div>
                    </li>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/exercise.png')"></span>
                            <p class="para-info mb-0 p-2 ml-3">Pool Overlooking the Hills</p>
                        </div>
                    </li>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/house.png')"></span>
                            <p class="para-info mb-0 p-2 ml-3">Guided Tours to our Oxganic Farm</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php') ?>
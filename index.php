<?php include('header.php') ?>
<div class="main-banner">

    <div class="banner-text">
        <h1 class="large-text mb-3 largetxtdsk">Soak in<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;the nature</h1>
        <h1 class="large-text mb-3 largetxtmob">Soak in<br>&nbsp;&nbsp;&nbsp;&nbsp;the nature</h1>
        <h3 class="heading-h3 mb-5">Memories that will last forever</h3>

        <center>
            <div class="btn btn-primary text-center vmorebtn">View More</div>
        </center>
    </div>
</div>

<div class="welcome-section pd-100 amibgclr">
    <div class="container">
        <p class="small-heading">Welcome</p>
        <h4 class="heading-h4 mb-4">This ones for your <br>Bucket List</h4>
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12">
                <p class="para-info">Forest Falls Resort is a 10 acre farm land, just around the corner from Mumbai situated in the shadows of the mighty Sahyadri and next to a calm Ulhas river.
                    With over 2500 plantations, this farm land has been converted into a Resort keeping all natural aspects in check. The rustic feel of our farm-resort is unlike any you may have come across.
                    From de-stressing nooks to nature trails, we offer a one-of-a-kind experience that gives you the holiday that you seek for a truly unique time.
                </p>
                <p class="para-info boldline">Our resort is pet friendly and is absolutely loved by furry buddies!</p>
                <a href="aboutus.php"class="desktopbtn">
                    <div class="vbtn mt-3">
                            <div class="btn btn-primary text-center vmorebtn">View More</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <p class="para-info">Our Villas and Cottages are tastefully decorated with your comfort in mind.
                    For that quick getaway from Mumbai and Pune, Forest Falls is one of the finest locations for Day/Stay picnics and just about any event!
                    Keeping the theme of the surrounding as natural as possible, this place offers rustic views and treasured moments. Indulge in outdoor and indoor sports or walk around the local village and be an explorer. This place will not cease to surprise you.
                </p>

                <a href="aboutus.php" class="mobbtn">
                    <div class="vbtn mt-3">
                            <div class="btn btn-primary text-center vmorebtn">View More</div>
                    </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="welcome-img"></div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="places pdtb-50">

    <p class="small-heading text-center">OUR SPECIALS</p>
    <h4 class="heading-h4 mb-4 text-center">Where Simple Luxury & <br>Tranquility Meet</h4>
 
            <nav>
                <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
                    <ul class="specialslist">
                        <li><a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">A Wedding Wow</a></li>
                        <li><a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Filming & Photo shoots</a></li>
                        <li> <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Corporate Off-site & Team Building</a></li>
                        <li><a class="nav-item nav-link" id="nav-contact-tab1" data-toggle="tab" href="#nav-contact1" role="tab" aria-controls="nav-contact1" aria-selected="false">Extended Stays</a></li>
                    </ul>
                </div>
            </nav>


    <div class="row m-0">
        <div class="col-lg-12 col-md-12 col-sm-12 p-0">
            <div class="tab-content p-0" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <div class="wedding-wow">
                        <div class="wedding-wowbox">
                            <div class="wowboxdetails p-5">
                                <h4 class="heading-h4 mb-4">A Wedding Wow</h4>
                                <p class="para-info py-2">
                                    A rustic setup adorned with greenery will be the perfect theme wedding one can have. While our open fields will be quintessential for the ceremony, our accommodation, with a capacity of 100, will comfort all the guests
                                </P>
                                <a href="events.php"><span class="viewmre"> VIEW MORE -></span></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="wedding-wow flimingbgbox">
                        <div class="wedding-wowbox">
                            <div class="wowboxdetails p-5">
                                <h4 class="heading-h4 mb-4">Filming & Photo shoots</h4>
                                <p class="para-info py-2">
                                    A rustic setup adorned with greenery will be the perfect theme wedding one can have. While our open fields will be quintessential for the ceremony, our accommodation, with a capacity of 100, will comfort all the guests
                                </P>
                                <a href="events.php"><span class="viewmre"> VIEW MORE -></span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <div class="wedding-wow copratebox">
                        <div class="wedding-wowbox">
                            <div class="wowboxdetails p-5">
                                <h4 class="heading-h4 mb-4">Corporate Off-site & Team Building</h4>
                                <p class="para-info py-2">
                                    A rustic setup adorned with greenery will be the perfect theme wedding one can have. While our open fields will be quintessential for the ceremony, our accommodation, with a capacity of 100, will comfort all the guests
                                </P>
                                <a href="events.php"><span class="viewmre"> VIEW MORE -></span></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="nav-contact1" role="tabpanel" aria-labelledby="nav-contact-tab1">
                    <div class="wedding-wow extendeddysbox">
                        <div class="wedding-wowbox">
                            <div class="wowboxdetails p-5">
                                <h4 class="heading-h4 mb-4">Extended Stays</h4>
                                <p class="para-info py-2">
                                    A rustic setup adorned with greenery will be the perfect theme wedding one can have. While our open fields will be quintessential for the ceremony, our accommodation, with a capacity of 100, will comfort all the guests
                                </P>
                                <span class="viewmre"> VIEW MORE -></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="roomnsuites pdtb-50">
    <div class="container">
        <p class="small-heading text-center dmreg">DISCOVER</p>
        <h4 class="heading-h4 mb-4 text-center">Rooms & Suites</h4>

        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-12">
                <div class="d-flex">
                    <div class="lrgeimgbx dlxrm m-2">
                        <div class="layer"></div>
                        <p class="roomdtl m-0 p-4"><b>Deluxe AC/Non AC</b><br>
                            <span class="dmreg">from</span> <span class="rmprice">2700 INR/Per Person</span>
                        </p>
                    </div>

                </div>
            </div>

            <div class="col-lg-8 co-md-12">

                <div class="smlimggrp">

                    <div class="d-flex">
                        <div class="slimg supacrm m-2">
                            <div class="layer"></div>
                            <p class="roomdtl m-0 p-4"><b>Superior AC/Non AC</b><br>
                                <span class="dmreg">from</span> <span class="rmprice">2500 INR/Per Person</span>
                            </p>
                        </div>

                        <div class="slimg m-2">
                            <div class="layer"></div>
                            <p class="roomdtl m-0 p-4"><b>AC /Non AC</b><br>
                                from<span class="rmprice"> 1200 INR/ 9am to 6pm</span>
                            </p>
                        </div>
                    </div>


                    <div class="d-flex">
                        <div class="slimg  fmlct m-2">
                            <div class="layer"></div>
                            <p class="roomdtl m-0 p-4"><b>Family Cottage for a Couple</b><br>
                                from <span class="rmprice">3000 INR/Per Person</span>
                            </p>
                        </div>

                        <div class="slimg m-2">
                            <div class="layer"></div>
                            <p class="roomdtl m-0 p-4"><b>Family cottage for a couple </b><br>
                                from <span class="rmprice">1750 INR/ 9am to 6pm</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div><a href="roomsandsuites.php">
                    <div class="btn btn-primary text-center vmorebtn">View More</div>
                </a></div>
        </div>
    </div>
</div>

<div class="joinussection">
    <div class="col-lg-12">
        <div class="row">
            <div class="joinusimg">
                <div class="joinustext text-center">
                    <h1 class="large-text mb-3">Joins Us</h1>
                    <h3 class="heading-h3 mb-5 dmreg">Be Our Guest and Make Every Stay Memorable</h3>
                    <a href="contactus.php">
                        <div class="btn btn-primary text-center vmorebtn">join us now</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script
  src="https://code.jquery.com/jquery-3.5.1.slim.js"
  integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM="
  crossorigin="anonymous"></script> -->
<!-- <script type="text/javascript">
    $( document ).ready(function() {
        $(".nav-tabs li a").click(function(){
  $(".nav-tabs li a").removeClass("active");
  $(this).addClass("active");
});
});
   
</script> -->

<?php include('footer.php') ?>
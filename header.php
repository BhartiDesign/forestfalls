<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forest Falls - Fork In The Nature</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>

    <header class="main-header">
        <div class="cline">
            <div class="container">
                <div class="contact-line d-flex justify-content-between p-3">
                    <p class="phone-no m-0"><a href="tel:Call Us : 9820468844">Call Us : 9820468844/55</a></p>
                    <p class="bookstay m-0"><a href="contactus.php">BOOK YOUR STAY -></a></p>
                </div>
            </div>
        </div>
        <div class="headercon">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                        <ul class="navbar-nav align-items-center">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="aboutus.php">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="roomsandsuites.php">Rooms</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <div class="logo"></div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="events.php">Events</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="amenities.php">Amenities</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contactus.php">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>


    </header>
<?php include('header.php') ?>
<div class="evenetsbg">
    <div class="bannerbg d-flex evntbg">
        <div class="bnnertbox text-center">
            <h1 class="large-text">Events</h1>
        </div>
    </div>
</div>


<div class="amibgclr">
<div class="container eventsec py-5">
    <div class="evenetshead text-center pb-5">
        <h4 class="heading-h4 mb-3">Events</h4>
        <p class="para-info">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> Lorem Ipsum has been the industry's standard dummy
        </p>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row align-items-center">

                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="eventsbnner wwevent"></div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="evenetinfo">
                            <h3 class="heading-h3 pt-4">A Wedding Wow </h3>
                            <p class="para-info py-3">
                                A rustic setup adorned with greenery will be the perfect theme wedding one can have. While our open fields will be quintessential for the ceremony, our accommodation, with a capacity of 100, will comfort all the guests.
                            </p>
                        </div>
                    </div>


                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="eventsbnner fpevnt"></div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="evenetinfo">
                            <h3 class="heading-h3 pt-4">Filming & Photo shoots </h3>
                            <p class="para-info py-3">
                            We are not just limited to theme weddings but also host events like Reunions, Engagements, Pre-wedding photo shoots, Maternity shoots, Bachelor- Hens and Birthday parties. Our location is off beat and will surely add that zing to your celebrations. We are a venue suited for filming web series, short films and music videos. Showcasing your products / models amidst nature would be an added advantage with great lighting and natural backdrops
                            </p>
                        </div>
                    </div>



                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="eventsbnner cotbevent"></div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="evenetinfo">
                            <h3 class="heading-h3 pt-4">Corporate Off-site & <br>Team Building</h3>
                            <p class="para-info py-3">
                            Away from the chaotic corporate world, conduct a whole new meeting for your team at our resort. An exotic offsite for your employees in nature, will help in team building, increasing productivity, camaraderie and creativity. 
                            </p>
                        </div>
                    </div>


               
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="eventsbnner edevent"></div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="evenetinfo">
                            <h3 class="heading-h3 pt-4">Extended Stays</h3>
                            <p class="para-info py-3">
                            Step out of the surrounding four walls and escape to the free spirited life. With nature’s healing touch and the comfort of our resorts resources, feel recharged and rejuvenated after spending a few weeks with us. Your new work from home destination! Eat, pray, love, exercise, explore, engage and achieve that clarity you’ve always desired.
                            </p>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>
</div>



<?php include('footer.php') ?>
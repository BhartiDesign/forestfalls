<?php include('header.php') ?>
<div class="bannerbg abtbnr d-flex">
    <div class="bnnertbox">
        <h1 class="large-text">about us</h1>
    </div>
</div>

<div class="abtbg">
    <div class="container">
        <div class="col-lg-12">
            <div class="row py-5 align-items-center">
                <div class="col-lg-6">
                    <p class="small-heading">About us</p>
                    <h4 class="heading-h4 mb-4">Relax, Rewind and Refresh is our mantra at Forest Falls Resort.</h4>
                </div>

                <div class="col-lg-6">
                    <p class="para-info">
                        Forest Falls Resort, a rustic destination that celebrates nature, is spread over 10 acres, today converted into a lush green resort. Keeping the natural finesse philosophy in mind, the Resort lives up to its rustic roots of keeping everything natural. The pathways, vegetation and scenes are unfiltered and give a cozy yet fresh vibe.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="faclisection">
    <div class="container py-5">
        <div class="col-lg-12 py-3 text-center">
            <p class="small-heading dmreg">OUR SERVICES</p>
            <h4 class="heading-h4 mb-4">Facilities & Service</h4>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between align-items-center mb-3">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/swimmer.png')"></span>
                            <p class="para-info px-5 m-0">The resort shares its boundary with Ulhas River, suitable for fishing and swimming. A perfect spot for sunrise and sunset too!</p>
                        </div>
                    </li>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between align-items-center mb-3">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/exercise.png')"></span>
                            <p class="para-info px-5 m-0">An enchanting cave temple of the ancestral deity makes a perfect setting for some mindfulness, meditation and self-discovery.</p>
                        </div>
                    </li>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between align-items-center mb-3">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/house.png')"></span>
                            <p class="para-info px-5 m-0">Deluxe and Superior Cottages are built to comfortably accommodate 10-15 pax. Family Rooms accommodate 4-6 pax..</p>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12">
                <ul>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between align-items-center mb-3">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/sunbed.png')"></span>
                            <p class="para-info px-5 m-0">The Resort comes with an open air pool, a dip pool, rain-dance and water slides. A huge waterfall is the resorts' USP.</p>
                        </div>
                    </li>
                    <li>
                        <div class="faclitiesbox d-flex justify-content-between align-items-center mb-3">
                            <span class="gbox"></span>
                            <span class="iconfac" style="background:url('./img/people.png')"></span>
                            <p class="para-info px-5 m-0">The resort can accommodate over 100 people for an overnight stay and 400 people for a one day sojourn..</p>
                        </div>
                    </li>
                    <li>
                        <div class="faclitiesbox d-flex  align-items-center mb-3">
                            <span class="gbox"></span>
                            <span class="iconfac climg" style="margin-left: -15px;background:url('./img/cutlery.png');"></span>
                            <p class="para-info pl-3 pr-5 m-0">All meals are laid out in a buffet. </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php') ?>
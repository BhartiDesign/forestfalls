<?php include('header.php') ?>
<div class="romnsuitbg">
    <div class="bannerbg  roomsbg d-flex">
        <div class="bnnertbox text-center">
            <h1 class="large-text">Rooms & Suites</h1>
        </div>
    </div>
</div>


<div class="amibgclr">
<div class="container">
        <div class="row">
            <div class="staydetails text-center w-100 py-5">
                <h3 class="heading-h3 fntblck">All Inclusive</h3>
                <h4 class="heading-h4">Overnight stay package</h4>
            </div>
            <div class="col-lg-12 mb-3">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="roomdetailsbox">
                            <div class="roomimgs dlxrm"></div>
                            <div class="rentdetils p-3">
                                <h3 class="heading-h3 fntblck text-left">Deluxe AC | Non Ac</h3>
                                <p class="rateprice m-0 dmreg">₹ 2700/3000 </p>
                                <p class="rateprice pperosn m-0 dmreg">Rate per person</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="roomdetailsbox">
                            <div class="roomimgs supacrm"></div>
                            <div class="rentdetils p-3">
                                <h3 class="heading-h3 fntblck text-left">Superior AC | Non AC</h3>
                                <p class="rateprice m-0 dmreg">₹ 2700/2500 </p>
                                <p class="rateprice pperosn m-0 dmreg">Rate per person</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="roomdetailsbox">
                            <div class="roomimgs fmlct"></div>
                            <div class="rentdetils p-3">
                                <h3 class="heading-h3 fntblck text-left">Family Cottage </h3>
                                <p class="rateprice m-0 dmreg">₹ 3500/3000 </p>
                                <p class="rateprice pperosn m-0 dmreg">Rate per person</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="staydetails text-center w-100 py-5 ondaypicnic">
                <h3 class="heading-h3 fntblck">All Inclusive</h3>
                <h4 class="heading-h4">One day picnic package</h4>
                <p class="timepicnic">9 am to 6 pm</p>
            </div>
            <div class="col-lg-12 mb-5">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="roomdetailsbox">
                            <div class="roomimgs dlxrm"></div>
                            <div class="rentdetils p-3">
                                <h3 class="heading-h3 fntblck text-left">Deluxe AC | Non Ac</h3>
                                <p class="rateprice m-0">₹ 1500/1200 </p>
                                <p class="rateprice pperosn m-0">Rate per person</p>
                                <p class="rateprice pperosn m-0">9 am to 6 pm</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <div class="roomdetailsbox">
                            <div class="roomimgs dlxrm"></div>
                            <div class="rentdetils p-3">
                                <h3 class="heading-h3 fntblck text-left">Deluxe AC | Non Ac</h3>
                                <p class="rateprice m-0">₹ 2000/1750 </p>
                                <p class="rateprice pperosn m-0">Rate per person</p>
                                <p class="rateprice pperosn m-0">9 am to 6 pm</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
   
    <div class="whitebg"> 
    <div class="container">
        <div class="row">
            <div class="col-lg-12 pd-100 ressec">
                <div class="row">
                    <div class="col-lg-4 px-2">
                        <div class="fooddetails">
                            <p class="small-heading">RESTAURANTS</p>
                            <h4 class="heading-h4 mb-4">Deliciously From <br>Our Chefs</h4>
                            <div class="dishimg"></div>
                        </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6">
                                <p class="para-info py-2">Rates are inclusive of meals, accommodation and usage of all facilities.
                                    Meals are served in buffet style with Vegetarian and Non Vegetarian options in our open air restaurant.</p>

                                <p class="para-info py-2">
                                    Jain, baby and pet food available on prior request
                                    Appetizers and mineral water can be ordered at cost. Guests are allowed to carry their own drinks.

                                </p>
                            </div>

                            <div class="col-lg-6">
                                <h3 class="heading-h3 fntblck text-left">Meal timings</h3>
                                <ul>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Breakfast : 9:30 am - 11.00 am</li>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Lunch: 1.30 pm - 3.00 pm</li>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Evening Tea & Snacks: 5.30 pm - 6.30 pm</li>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Dinner: 9.00 pm - 10.30 pm</li>
                                </ul>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 mt-5">
                                <h3 class="heading-h3 fntblck text-left">Terms & Conditions:</h3>
                                <ul>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Reservations over the phone on 9820468844/55</li>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Booking will be confirmed by 50% advance payment via Netbanking, Google pay, Paytm</li>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Balance payment to be made at check-in</li>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Booking made can be postponed to a future date, if available</li>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Cancellation allowed if it is 3 days prior to confirmed date</li>
                                    <li class="para-info mb-2"><span class="listdiamind"></span> Cancellation charges Rs.500</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
</div>


<?php include('footer.php') ?>